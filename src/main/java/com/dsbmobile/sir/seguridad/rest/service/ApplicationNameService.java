package com.dsbmobile.sir.seguridad.rest.service;

/**
 *
 * @author otheo
 */
public interface ApplicationNameService {

    boolean existe(String applicationName);

}
