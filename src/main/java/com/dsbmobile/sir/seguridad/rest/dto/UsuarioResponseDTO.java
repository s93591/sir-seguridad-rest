package com.dsbmobile.sir.seguridad.rest.dto;

/**
 *
 * @author otheo
 */
public class UsuarioResponseDTO extends BaseDTO {

    private static final long serialVersionUID = 4791783858499170241L;

    private String id;
    private String displayName;
    private String userPrincipalName;
    private String correoElectronico;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getUserPrincipalName() {
        return userPrincipalName;
    }

    public void setUserPrincipalName(String userPrincipalName) {
        this.userPrincipalName = userPrincipalName;
    }

    public String getCorreoElectronico() {
        return correoElectronico;
    }

    public void setCorreoElectronico(String correoElectronico) {
        this.correoElectronico = correoElectronico;
    }

    @Override
    public String toString() {
        return "UsuarioResponseDTO{" + "id=" + id + ", displayName=" + displayName + ", userPrincipalName=" + userPrincipalName + ", correoElectronico=" + correoElectronico + '}';
    }

}
