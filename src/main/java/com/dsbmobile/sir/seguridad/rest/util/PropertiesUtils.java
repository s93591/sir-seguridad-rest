package com.dsbmobile.sir.seguridad.rest.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author otheo
 */
public final class PropertiesUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(PropertiesUtils.class);
    private static final Properties PROPERTIES;
    private static final String PROPERTIES_PATH = "properties/sir-seguridad-rest/sir-seguridad-rest.properties";

    static {
        PROPERTIES = new Properties();
        try {
            InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream(PROPERTIES_PATH);
            PROPERTIES.load(is);
        } catch (IOException ex) {
            LOGGER.error("Error al inicializar PropertiesUtils", ex);
        } catch (Exception ex) {
            LOGGER.error("Error al inicializar PropertiesUtils", ex);
        }
    }

    private PropertiesUtils() {
    }

    public static String getProperty(String name) {
        return (PROPERTIES == null) ? null : PROPERTIES.getProperty(name);
    }

}
