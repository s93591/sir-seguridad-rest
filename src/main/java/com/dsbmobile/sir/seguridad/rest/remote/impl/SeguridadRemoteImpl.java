package com.dsbmobile.sir.seguridad.rest.remote.impl;

import com.dsbmobile.sir.seguridad.rest.enums.ErrorCodeEnum;
import com.dsbmobile.sir.seguridad.rest.enums.PropertiesEnum;
import com.dsbmobile.sir.seguridad.rest.remote.SeguridadRemote;
import com.dsbmobile.sir.seguridad.rest.ro.in.AutenticarInRO;
import com.dsbmobile.sir.seguridad.rest.ro.out.AutenticarOutRO;
import com.dsbmobile.sir.seguridad.rest.service.ApplicationNameService;
import com.dsbmobile.sir.seguridad.rest.service.UsuarioService;
import com.dsbmobile.sir.seguridad.rest.util.PropertiesUtils;
import com.dsbmobile.sir.seguridad.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author otheo
 */
@Service(value = "seguridadRemote")
public class SeguridadRemoteImpl implements SeguridadRemote {

    private static final Logger APPACCESS_LOGGER = LoggerFactory.getLogger(PropertiesUtils.getProperty(PropertiesEnum.APPACCESS_LOGGER_NAME.getValue()));

    @Autowired
    private ApplicationNameService applicationNameService;
    @Autowired
    private UsuarioService usuarioService;

    @Override
    public AutenticarOutRO autenticar(AutenticarInRO in) {
        System.out.println("[{}] llamando servicio /seguridad/autenticar - " + in.getApplicationName()); //TODO para prueba, eliminar
        System.out.println("INPUT: {} - " + in.toString()); //TODO para prueba, eliminar
        
        if (in == null || StringUtils.isEmpty(in.getApplicationName())) {
            AutenticarOutRO out = new AutenticarOutRO();
            out.setError(ErrorCodeEnum.NOMBRE_APP_REQUERIDO);
            return out;
        } else {
            if (!applicationNameService.existe(in.getApplicationName())) {
                AutenticarOutRO out = new AutenticarOutRO();
                out.setError(ErrorCodeEnum.NOMBRE_APP_INVALIDO);
                return out;
            }
        }
        APPACCESS_LOGGER.info("[{}] llamando servicio /seguridad/autenticar", in.getApplicationName());
        APPACCESS_LOGGER.info("INPUT: {}", in.toString());
        AutenticarOutRO out = usuarioService.autenticar(in);
        APPACCESS_LOGGER.info("OUTPUT: {}", out.toString());
        return out;
    }

}
