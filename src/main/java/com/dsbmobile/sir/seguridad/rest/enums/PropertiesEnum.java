package com.dsbmobile.sir.seguridad.rest.enums;

import com.dsbmobile.sir.seguridad.util.StringUtils;

/**
 *
 * @author otheo
 */
public enum PropertiesEnum {

    APPACCESS_LOGGER_NAME("appaccess.logger.name"),
    USUARIO_REST("usuario.rest"),
    AUTENTICACION_URL("autenticacion.url"),
    AUTENTICACION_TENANT_NAME("autenticacion.tenant.name"),
    AUTENTICACION_PARAM_GRANTTYPE("autenticacion.param.grantType"),
    AUTENTICACION_PARAM_CLIENTID("autenticacion.param.clientId"),
    AUTENTICACION_PARAM_CLIENTSECRET("autenticacion.param.clientSecret"),
    AUTENTICACION_PARAM_RESOURCE("autenticacion.param.resource"),
    AUTENTICACION_PARAM_MASTERUSERNAME("autenticacion.param.masterUsername"),
    AUTENTICACION_PARAM_MASTERPASSWORD("autenticacion.param.masterPassword"),
    LISTAR_GRUPOS_URL("listar.grupos.url"),
    LISTAR_GRUPOS_FILTRO("listar.grupos.filtro"),
    LISTAR_USUARIOS_URL("listar.usuarios.url");

    private final String value;

    private PropertiesEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static PropertiesEnum get(String value) {
        if (!StringUtils.isEmpty(value)) {
            for (PropertiesEnum e : PropertiesEnum.values()) {
                if (e.getValue().equals(value)) {
                    return e;
                }
            }
        }
        return null;
    }

}
