package com.dsbmobile.sir.seguridad.rest.dto;

/**
 *
 * @author otheo
 */
public class GrupoResponseDTO extends BaseDTO {

    private static final long serialVersionUID = 3229270016850830011L;

    private String id;
    private String displayName;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    @Override
    public String toString() {
        return "GrupoResponseDTO{" + "id=" + id + ", displayName=" + displayName + '}';
    }

}
