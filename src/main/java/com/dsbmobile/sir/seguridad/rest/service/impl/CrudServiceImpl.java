package com.dsbmobile.sir.seguridad.rest.service.impl;

import com.dsbmobile.sir.seguridad.rest.service.CrudService;
import com.dsbmobile.sir.seguridad.rest.util.Constantes;
import com.dsbmobile.sir.seguridad.util.exception.BusinessException;
import java.util.Map;
import java.util.Set;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author otheo
 */
@Repository(value = "crudService")
@Transactional
public class CrudServiceImpl implements CrudService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CrudServiceImpl.class);

    @PersistenceContext(unitName = Constantes.PERSISTENCE_UNIT_NAME)
    private EntityManager em;

    @Override
    public EntityManager getEm() {
        return em;
    }

    @Override
    public <T> T find(Object id, Class<T> type) throws BusinessException {
        try {
            LOGGER.info("buscando: {}, id: {}", type.getName(), id.toString());
            return em.find(type, id);
        } catch (Exception ex) {
            throw new BusinessException("Error al buscar la entidad por id.", ex);
        }
    }

    @Override
    public <T> T create(T t) throws BusinessException {
        try {
            LOGGER.info("insertando: {}", t.getClass().getName());
            em.persist(t);
            em.flush();
            em.refresh(t);
            return t;
        } catch (Exception ex) {
            throw new BusinessException("Error al crear la entidad.", ex);
        }
    }

    @Override
    public <T> T update(T t) throws BusinessException {
        try {
            LOGGER.info("actualizando: {}", t.getClass().getName());
            return em.merge(t);
        } catch (Exception ex) {
            throw new BusinessException("Error al actualizar la entidad.", ex);
        }
    }

    @Override
    public void delete(Object t) throws BusinessException {
        try {
            LOGGER.info("eliminando: {}", t.getClass().getName());
            em.remove(t);
            em.flush();
        } catch (Exception ex) {
            throw new BusinessException("Error al eliminar la entidad.", ex);
        }
    }

    @Override
    public Query createNativeQuery(String nativeQuery, Map<String, Object> parameters) throws BusinessException {
        return createQuery(nativeQuery, parameters, TipoQuery.NATIVE);
    }

    @Override
    public Query createJPQL(String jpql, Map<String, Object> parameters) throws BusinessException {
        return createQuery(jpql, parameters, TipoQuery.JPQL);
    }

    @Override
    public Query createNamedQuery(String namedQuery, Map<String, Object> parameters) throws BusinessException {
        return createQuery(namedQuery, parameters, TipoQuery.NAMED);
    }

    private Query createQuery(String string, Map<String, Object> parameters, TipoQuery tipo) throws BusinessException {
        try {
            LOGGER.info("generando query: {}, con par\u00E1metros: {}", string, parameters);
            Query query;
            switch (tipo) {
                case NATIVE:
                    query = em.createNativeQuery(string);
                    break;
                case JPQL:
                    query = em.createQuery(string);
                    break;
                case NAMED:
                    query = em.createNamedQuery(string);
                    break;
                default:
                    throw new IllegalStateException("Error en TipoQuery.");
            }
            if (parameters != null) {
                Set<Map.Entry<String, Object>> rawParameters = parameters.entrySet();
                for (Map.Entry<String, Object> entry : rawParameters) {
                    query.setParameter(entry.getKey(), entry.getValue());
                }
            }
            return query;
        } catch (IllegalStateException ex) {
            throw new BusinessException("Error al generar query.", ex);
        }
    }

    @Override
    public Query createNativeQuery(String nativeQuery) throws BusinessException {
        return createNativeQuery(nativeQuery, null);
    }

    @Override
    public Query createJPQL(String jpql) throws BusinessException {
        return createJPQL(jpql, null);
    }

    @Override
    public Query createNamedQuery(String namedQuery) throws BusinessException {
        return createNamedQuery(namedQuery, null);
    }

    private enum TipoQuery {
        NATIVE, JPQL, NAMED;
    }

}
