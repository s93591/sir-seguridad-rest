package com.dsbmobile.sir.seguridad.rest.service;

import com.dsbmobile.sir.seguridad.rest.ro.base.BaseInRO;
import com.dsbmobile.sir.seguridad.rest.ro.base.BaseOutRO;
import com.dsbmobile.sir.seguridad.rest.ro.in.AutenticarInRO;
import com.dsbmobile.sir.seguridad.rest.ro.out.AutenticarOutRO;

/**
 *
 * @author otheo
 */
public interface UsuarioService {

    AutenticarOutRO autenticar(AutenticarInRO in);

    BaseOutRO sincronizarUsuarios(BaseInRO in);
    
    boolean existe(String uniqueId);
    
}
