package com.dsbmobile.sir.seguridad.rest.service;

import com.dsbmobile.sir.seguridad.util.exception.BusinessException;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author otheo
 */
public interface CrudService {

    EntityManager getEm();

    <T> T find(Object id, Class<T> type) throws BusinessException;

    <T> T create(T t) throws BusinessException;

    <T> T update(T t) throws BusinessException;

    void delete(Object t) throws BusinessException;

    Query createNativeQuery(String nativeQuery, Map<String, Object> parameters) throws BusinessException;

    Query createJPQL(String jpql, Map<String, Object> parameters) throws BusinessException;

    Query createNamedQuery(String namedQuery, Map<String, Object> parameters) throws BusinessException;

    Query createNativeQuery(String nativeQuery) throws BusinessException;

    Query createJPQL(String jpql) throws BusinessException;

    Query createNamedQuery(String namedQuery) throws BusinessException;

}
