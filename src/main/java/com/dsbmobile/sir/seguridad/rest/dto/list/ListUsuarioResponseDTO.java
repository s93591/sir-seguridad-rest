package com.dsbmobile.sir.seguridad.rest.dto.list;

import com.dsbmobile.sir.seguridad.rest.dto.BaseDTO;
import com.dsbmobile.sir.seguridad.rest.dto.UsuarioResponseDTO;
import java.util.List;

/**
 *
 * @author otheo
 */
public class ListUsuarioResponseDTO extends BaseDTO {

    private static final long serialVersionUID = -6641330572521162417L;

    private List<UsuarioResponseDTO> usuarios;

    public List<UsuarioResponseDTO> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(List<UsuarioResponseDTO> usuarios) {
        this.usuarios = usuarios;
    }

    @Override
    public String toString() {
        return "ListUsuarioResponseDTO{" + "usuarios=" + usuarios + '}';
    }

}
