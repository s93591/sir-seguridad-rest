package com.dsbmobile.sir.seguridad.rest.service.impl;

import com.dsbmobile.sir.seguridad.rest.dto.AutenticacionResponseDTO;
import com.dsbmobile.sir.seguridad.rest.dto.GrupoResponseDTO;
import com.dsbmobile.sir.seguridad.rest.dto.UsuarioResponseDTO;
import com.dsbmobile.sir.seguridad.rest.dto.list.ListGrupoResponseDTO;
import com.dsbmobile.sir.seguridad.rest.dto.list.ListUsuarioResponseDTO;
import com.dsbmobile.sir.seguridad.rest.enums.PropertiesEnum;
import com.dsbmobile.sir.seguridad.rest.service.AdService;
import com.dsbmobile.sir.seguridad.rest.util.Constantes;
import com.dsbmobile.sir.seguridad.rest.util.JerseyJsonProvider;
import com.dsbmobile.sir.seguridad.rest.util.PropertiesUtils;
import com.dsbmobile.sir.seguridad.util.StringUtils;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

/**
 *
 * @author otheo
 */
@Service(value = "adService")
public class AdServiceImpl implements AdService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AdServiceImpl.class);

    @Override
    public AutenticacionResponseDTO autenticar(String usuario, String contrasenia) {
        AutenticacionResponseDTO dto = new AutenticacionResponseDTO();
        try {
            MultivaluedMap<String, String> multivaluedMap = new MultivaluedMapImpl();
            multivaluedMap.add(Constantes.AUTENTICACION_GRANT_TYPE_KEY, PropertiesUtils.getProperty(PropertiesEnum.AUTENTICACION_PARAM_GRANTTYPE.getValue()));
            multivaluedMap.add(Constantes.AUTENTICACION_CLIENT_ID_KEY, PropertiesUtils.getProperty(PropertiesEnum.AUTENTICACION_PARAM_CLIENTID.getValue()));
            multivaluedMap.add(Constantes.AUTENTICACION_CLIENT_SECRET_KEY, PropertiesUtils.getProperty(PropertiesEnum.AUTENTICACION_PARAM_CLIENTSECRET.getValue()));
            multivaluedMap.add(Constantes.AUTENTICACION_RESOURCE_KEY, PropertiesUtils.getProperty(PropertiesEnum.AUTENTICACION_PARAM_RESOURCE.getValue()));
            multivaluedMap.add(Constantes.AUTENTICACION_USERNAME_KEY, usuario);
            multivaluedMap.add(Constantes.AUTENTICACION_PASSWORD_KEY, contrasenia);

            ClientConfig clientConfig = new DefaultClientConfig();
            clientConfig.getClasses().add(JerseyJsonProvider.class);
            Client client = Client.create(clientConfig);

            String url = PropertiesUtils.getProperty(PropertiesEnum.AUTENTICACION_URL.getValue());
            url = url.replace(Constantes.TENANT_NAME_PLACEHOLDER, PropertiesUtils.getProperty(PropertiesEnum.AUTENTICACION_TENANT_NAME.getValue()));
            WebResource webResource = client.resource(url);

            ClientResponse clientResponse = webResource
                    .queryParams(multivaluedMap)
                    .type(MediaType.APPLICATION_FORM_URLENCODED_TYPE)
                    .post(ClientResponse.class, multivaluedMap);

            LOGGER.info("STATUS=" + clientResponse.getStatus());
            LOGGER.info("STATUS INFO=" + clientResponse.getStatusInfo().getReasonPhrase());
            if (clientResponse.getStatus() != Constantes.HTTP_OK) {
                dto.setError(true);
                dto.setMessage("Status : " + clientResponse.getStatus() + " Reason : " + clientResponse.getStatusInfo().getReasonPhrase());
            } else {
                Map<String, String> entity = clientResponse.getEntity(Map.class);
                String token = entity.get(Constantes.AUTENTICACION_ACCESS_TOKEN_KEY);
                if (StringUtils.isEmpty(token)) {
                    dto.setError(true);
                    dto.setMessage("No se pudo obtener el token de autenticaci\u00F3n.");
                } else {
                    dto.setToken(token);
                }
            }
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            dto.setError(true);
            dto.setMessage("Error en autenticaci\u00F3n. " + ex.getMessage());
        }
        return dto;
    }

    @Override
    public ListGrupoResponseDTO listarGrupos() {
        ListGrupoResponseDTO dto = new ListGrupoResponseDTO();
        try {
            AutenticacionResponseDTO autenticar = this.autenticar(PropertiesUtils.getProperty(PropertiesEnum.AUTENTICACION_PARAM_MASTERUSERNAME.getValue()),
                    PropertiesUtils.getProperty(PropertiesEnum.AUTENTICACION_PARAM_MASTERPASSWORD.getValue()));
            if (autenticar.tieneError()) {
                dto.setError(true);
                dto.setMessage(autenticar.getMessage());
            } else {
                String token = autenticar.getToken();
                dto.setToken(token);

                MultivaluedMap<String, String> multivaluedMap = new MultivaluedMapImpl();
                String filtroGrupo = PropertiesUtils.getProperty(PropertiesEnum.LISTAR_GRUPOS_FILTRO.getValue());
                String paramValue = Constantes.LISTAR_GRUPOS_PARAM_VALUE;
                paramValue = paramValue.replace(Constantes.GROUP_NAME_PLACEHOLDER, filtroGrupo);
                multivaluedMap.add(Constantes.LISTAR_GRUPOS_PARAM_KEY, paramValue);

                ClientConfig clientConfig = new DefaultClientConfig();
                clientConfig.getClasses().add(JerseyJsonProvider.class);
                Client client = Client.create(clientConfig);

                WebResource webResource = client.resource(PropertiesUtils.getProperty(PropertiesEnum.LISTAR_GRUPOS_URL.getValue()));

                ClientResponse clientResponse = webResource
                        .queryParams(multivaluedMap)
                        .accept(MediaType.WILDCARD_TYPE)
                        .header(Constantes.HEADER_AUTHORIZATION, token)
                        .get(ClientResponse.class);

                if (clientResponse.getStatus() != Constantes.HTTP_OK) {
                    dto.setError(true);
                    dto.setMessage("Status : " + clientResponse.getStatus() + " Reason : " + clientResponse.getStatusInfo().getReasonPhrase());
                } else {
                    Map<String, Object> entity = clientResponse.getEntity(Map.class);
                    if (entity != null) {
                        List<Map<String, Object>> grupos = (List<Map<String, Object>>) entity.get(Constantes.LISTAR_GRUPOS_ARRAY_NAME);
                        if (!CollectionUtils.isEmpty(grupos)) {
                            dto.setGrupos(new ArrayList<GrupoResponseDTO>());
                            for (Map<String, Object> grupo : grupos) {
                                GrupoResponseDTO gdto = new GrupoResponseDTO();
                                gdto.setId((String) grupo.get(Constantes.LISTAR_GRUPOS_ID_KEY));
                                gdto.setDisplayName((String) grupo.get(Constantes.LISTAR_GRUPOS_DISPLAYNAME_KEY));
                                dto.getGrupos().add(gdto);
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            dto.setError(true);
            dto.setMessage("Error al listar grupos. " + ex.getMessage());
        }
        return dto;
    }

    @Override
    public ListUsuarioResponseDTO listarUsuarios(String idGrupo) {
        return listarUsuarios(idGrupo, null);
    }

    @Override
    public ListUsuarioResponseDTO listarUsuarios(String idGrupo, String token) {
        ListUsuarioResponseDTO dto = new ListUsuarioResponseDTO();
        try {
            if (StringUtils.isEmpty(token)) {
                AutenticacionResponseDTO autenticar = this.autenticar(PropertiesUtils.getProperty(PropertiesEnum.AUTENTICACION_PARAM_MASTERUSERNAME.getValue()),
                        PropertiesUtils.getProperty(PropertiesEnum.AUTENTICACION_PARAM_MASTERPASSWORD.getValue()));
                if (autenticar.tieneError()) {
                    dto.setError(true);
                    dto.setMessage(autenticar.getMessage());
                    return dto;
                } else {
                    token = autenticar.getToken();
                }
            }

            ClientConfig clientConfig = new DefaultClientConfig();
            clientConfig.getClasses().add(JerseyJsonProvider.class);
            Client client = Client.create(clientConfig);

            String url = PropertiesUtils.getProperty(PropertiesEnum.LISTAR_USUARIOS_URL.getValue());
            url = url.replace(Constantes.GROUP_ID_PLACEHOLDER, idGrupo);
            WebResource webResource = client.resource(url);

            ClientResponse clientResponse = webResource
                    .accept(MediaType.WILDCARD_TYPE)
                    .header(Constantes.HEADER_AUTHORIZATION, token)
                    .get(ClientResponse.class);

            if (clientResponse.getStatus() != Constantes.HTTP_OK) {
                dto.setError(true);
                dto.setMessage("Status : " + clientResponse.getStatus() + " Reason : " + clientResponse.getStatusInfo().getReasonPhrase());
            } else {
                Map<String, Object> entity = clientResponse.getEntity(Map.class);
                if (entity != null) {
                    List<Map<String, Object>> usuarios = (List<Map<String, Object>>) entity.get(Constantes.LISTAR_USUARIOS_ARRAY_NAME);
                    if (!CollectionUtils.isEmpty(usuarios)) {
                        dto.setUsuarios(new ArrayList<UsuarioResponseDTO>());
                        for (Map<String, Object> usuario : usuarios) {
                            UsuarioResponseDTO udto = new UsuarioResponseDTO();
                            udto.setId((String) usuario.get(Constantes.LISTAR_USUARIOS_ID_KEY));
                            udto.setDisplayName((String) usuario.get(Constantes.LISTAR_USUARIOS_DISPLAYNAME_KEY));
                            udto.setUserPrincipalName((String) usuario.get(Constantes.LISTAR_USUARIOS_USERPRINCIPALNAME_KEY));
                            udto.setCorreoElectronico((String) usuario.get(Constantes.LISTAR_USUARIOS_MAIL_KEY));
                            dto.getUsuarios().add(udto);
                        }
                    }
                }
            }
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            dto.setError(true);
            dto.setMessage("Error al listar usuarios del grupo " + idGrupo + ". " + ex.getMessage());
        }
        return dto;
    }

}
