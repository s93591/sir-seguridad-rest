package com.dsbmobile.sir.seguridad.rest.util;

/**
 *
 * @author otheo
 */
public final class Constantes {

    public static final String UTF_8 = "UTF-8";
    public static final String PERSISTENCE_UNIT_NAME = "sirsegPU";

    public static final String TENANT_NAME_PLACEHOLDER = "{{tenantName}}";
    public static final String GROUP_ID_PLACEHOLDER = "{{groupId}}";
    public static final String GROUP_NAME_PLACEHOLDER = "{{groupName}}";
    
    public static final String HEADER_AUTHORIZATION = "Authorization";

    public static final String AUTENTICACION_GRANT_TYPE_KEY = "grant_type";
    public static final String AUTENTICACION_CLIENT_ID_KEY = "client_id";
    public static final String AUTENTICACION_CLIENT_SECRET_KEY = "client_secret";
    public static final String AUTENTICACION_RESOURCE_KEY = "resource";
    public static final String AUTENTICACION_USERNAME_KEY = "username";
    public static final String AUTENTICACION_PASSWORD_KEY = "password";
    public static final String AUTENTICACION_ACCESS_TOKEN_KEY = "access_token";

    public static final String LISTAR_GRUPOS_PARAM_KEY = "$filter";
    public static final String LISTAR_GRUPOS_PARAM_VALUE = "startswith(displayName,'{{groupName}}')";

    public static final String LISTAR_GRUPOS_ARRAY_NAME = "value";
    public static final String LISTAR_GRUPOS_ID_KEY = "id";
    public static final String LISTAR_GRUPOS_DISPLAYNAME_KEY = "displayName";

    public static final String LISTAR_USUARIOS_ARRAY_NAME = "value";
    public static final String LISTAR_USUARIOS_ID_KEY = "id";
    public static final String LISTAR_USUARIOS_DISPLAYNAME_KEY = "displayName";
    public static final String LISTAR_USUARIOS_USERPRINCIPALNAME_KEY = "userPrincipalName";
    public static final String LISTAR_USUARIOS_MAIL_KEY = "mail";
    
    public static final Integer HTTP_OK = 200;

    private Constantes() {
    }

}
