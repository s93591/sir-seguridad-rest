package com.dsbmobile.sir.seguridad.rest.dto.list;

import com.dsbmobile.sir.seguridad.rest.dto.BaseDTO;
import com.dsbmobile.sir.seguridad.rest.dto.GrupoResponseDTO;
import java.util.List;

/**
 *
 * @author otheo
 */
public class ListGrupoResponseDTO extends BaseDTO {

    private static final long serialVersionUID = -6686317812326046988L;

    private List<GrupoResponseDTO> grupos;
    private String token;

    public List<GrupoResponseDTO> getGrupos() {
        return grupos;
    }

    public void setGrupos(List<GrupoResponseDTO> grupos) {
        this.grupos = grupos;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return "ListGrupoResponseDTO{" + "grupos=" + grupos + ", token=" + token + '}';
    }

}
