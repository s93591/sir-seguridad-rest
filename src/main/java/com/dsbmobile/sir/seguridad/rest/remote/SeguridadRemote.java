package com.dsbmobile.sir.seguridad.rest.remote;

import com.dsbmobile.sir.seguridad.rest.ro.in.AutenticarInRO;
import com.dsbmobile.sir.seguridad.rest.ro.out.AutenticarOutRO;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author otheo
 */
public interface SeguridadRemote {

    @POST
    @Path("/autenticar")
    @Consumes(value = {MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Produces(value = {MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    AutenticarOutRO autenticar(AutenticarInRO in);

}
