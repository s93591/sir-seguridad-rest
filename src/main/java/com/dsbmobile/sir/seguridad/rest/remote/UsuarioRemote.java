package com.dsbmobile.sir.seguridad.rest.remote;

import com.dsbmobile.sir.seguridad.rest.ro.base.BaseInRO;
import com.dsbmobile.sir.seguridad.rest.ro.base.BaseOutRO;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author otheo
 */
public interface UsuarioRemote {

    @POST
    @Path("/sincronizarUsuarios")
    @Consumes(value = {MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Produces(value = {MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    BaseOutRO sincronizarUsuarios(BaseInRO in);

}
