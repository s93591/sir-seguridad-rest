package com.dsbmobile.sir.seguridad.rest.service;

import com.dsbmobile.sir.seguridad.rest.dto.AutenticacionResponseDTO;
import com.dsbmobile.sir.seguridad.rest.dto.list.ListGrupoResponseDTO;
import com.dsbmobile.sir.seguridad.rest.dto.list.ListUsuarioResponseDTO;

/**
 *
 * @author otheo
 */
public interface AdService {

    AutenticacionResponseDTO autenticar(String usuario, String contrasenia);

    ListGrupoResponseDTO listarGrupos();

    ListUsuarioResponseDTO listarUsuarios(String idGrupo);

    ListUsuarioResponseDTO listarUsuarios(String idGrupo, String token);

}
