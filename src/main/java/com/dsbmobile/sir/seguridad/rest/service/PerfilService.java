package com.dsbmobile.sir.seguridad.rest.service;

import com.dsbmobile.sir.seguridad.domain.Perfil;

/**
 *
 * @author otheo
 */
public interface PerfilService {

    boolean existe(String codPerfil);
    
    Perfil findByCodigo(String codPerfil);

}
