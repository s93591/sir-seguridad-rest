package com.dsbmobile.sir.seguridad.rest.dto;

import java.io.Serializable;

/**
 *
 * @author otheo
 */
public class BaseDTO implements Serializable {

    private static final long serialVersionUID = 3269804655184385355L;

    private Boolean error = false;
    private String message = "";

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean tieneError() {
        return this.error;
    }

    @Override
    public String toString() {
        return "BaseDTO{" + "error=" + error + ", message=" + message + '}';
    }

}
