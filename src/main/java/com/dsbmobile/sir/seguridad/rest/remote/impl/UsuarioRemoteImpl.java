package com.dsbmobile.sir.seguridad.rest.remote.impl;

import com.dsbmobile.sir.seguridad.rest.enums.ErrorCodeEnum;
import com.dsbmobile.sir.seguridad.rest.enums.PropertiesEnum;
import com.dsbmobile.sir.seguridad.rest.remote.UsuarioRemote;
import com.dsbmobile.sir.seguridad.rest.ro.base.BaseInRO;
import com.dsbmobile.sir.seguridad.rest.ro.base.BaseOutRO;
import com.dsbmobile.sir.seguridad.rest.service.ApplicationNameService;
import com.dsbmobile.sir.seguridad.rest.service.UsuarioService;
import com.dsbmobile.sir.seguridad.rest.util.PropertiesUtils;
import com.dsbmobile.sir.seguridad.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author otheo
 */
@Service(value = "usuarioRemote")
public class UsuarioRemoteImpl implements UsuarioRemote {

    private static final Logger APPACCESS_LOGGER = LoggerFactory.getLogger(PropertiesUtils.getProperty(PropertiesEnum.APPACCESS_LOGGER_NAME.getValue()));

    @Autowired
    private ApplicationNameService applicationNameService;
    @Autowired
    private UsuarioService usuarioService;

    @Override
    public BaseOutRO sincronizarUsuarios(BaseInRO in) {
        if (in == null || StringUtils.isEmpty(in.getApplicationName())) {
            BaseOutRO out = new BaseOutRO();
            out.setError(ErrorCodeEnum.NOMBRE_APP_REQUERIDO);
            return out;
        } else {
            if (!applicationNameService.existe(in.getApplicationName())) {
                BaseOutRO out = new BaseOutRO();
                out.setError(ErrorCodeEnum.NOMBRE_APP_INVALIDO);
                return out;
            }
        }
        APPACCESS_LOGGER.info("[{}] llamando servicio /usuario/sincronizarUsuarios", in.getApplicationName());
        APPACCESS_LOGGER.info("INPUT: {}", in.toString());
        BaseOutRO out = usuarioService.sincronizarUsuarios(in);
        APPACCESS_LOGGER.info("OUTPUT: {}", out.toString());
        return out;
    }

}
