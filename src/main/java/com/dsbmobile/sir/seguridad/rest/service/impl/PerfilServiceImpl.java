package com.dsbmobile.sir.seguridad.rest.service.impl;

import com.dsbmobile.sir.seguridad.domain.Perfil;
import com.dsbmobile.sir.seguridad.rest.service.CrudService;
import com.dsbmobile.sir.seguridad.rest.service.PerfilService;
import com.dsbmobile.sir.seguridad.util.QueryParameter;
import com.dsbmobile.sir.seguridad.util.exception.BusinessException;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

/**
 *
 * @author otheo
 */
@Repository(value = "perfilService")
@Transactional
public class PerfilServiceImpl implements PerfilService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PerfilServiceImpl.class);

    @Autowired
    private CrudService crudService;

    @Override
    public boolean existe(String codPerfil) {
        boolean result = false;
        try {
            Long cant = (Long) this.crudService.createNamedQuery(Perfil.EXISTE,
                    QueryParameter.with("codPerfil", codPerfil).parameters())
                    .getSingleResult();
            result = cant > 0;
        } catch (BusinessException ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
        return result;
    }

    @Override
    public Perfil findByCodigo(String codPerfil) {
        Perfil perfil = null;
        try {
            List<Perfil> resultList = this.crudService.createNamedQuery(Perfil.FIND_BY_COD_PERFIL,
                    QueryParameter.with("codPerfil", codPerfil).parameters()).getResultList();
            if (!CollectionUtils.isEmpty(resultList)) {
                perfil = resultList.get(0);
            }
        } catch (BusinessException ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
        return perfil;
    }

}
