package com.dsbmobile.sir.seguridad.rest.service.impl;

import com.dsbmobile.sir.seguridad.domain.ApplicationName;
import com.dsbmobile.sir.seguridad.rest.service.ApplicationNameService;
import com.dsbmobile.sir.seguridad.rest.service.CrudService;
import com.dsbmobile.sir.seguridad.util.QueryParameter;
import com.dsbmobile.sir.seguridad.util.exception.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author otheo
 */
@Repository(value = "applicationNameService")
@Transactional
public class ApplicationNameServiceImpl implements ApplicationNameService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationNameServiceImpl.class);

    @Autowired
    private CrudService crudService;

    @Override
    public boolean existe(String applicationName) {
        boolean result = false;
        try {
            Long cant = (Long) this.crudService.createNamedQuery(ApplicationName.EXISTE,
                    QueryParameter.with("applicationName", applicationName).parameters())
                    .getSingleResult();
            result = cant > 0;
        } catch (BusinessException ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
        return result;
    }

}
