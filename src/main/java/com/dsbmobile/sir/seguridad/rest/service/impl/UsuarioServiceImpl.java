package com.dsbmobile.sir.seguridad.rest.service.impl;

import com.dsbmobile.sir.seguridad.domain.DatosAuditoria;
import com.dsbmobile.sir.seguridad.domain.Perfil;
import com.dsbmobile.sir.seguridad.domain.Usuario;
import com.dsbmobile.sir.seguridad.rest.dto.AutenticacionResponseDTO;
import com.dsbmobile.sir.seguridad.rest.dto.GrupoResponseDTO;
import com.dsbmobile.sir.seguridad.rest.dto.UsuarioResponseDTO;
import com.dsbmobile.sir.seguridad.rest.dto.list.ListGrupoResponseDTO;
import com.dsbmobile.sir.seguridad.rest.dto.list.ListUsuarioResponseDTO;
import com.dsbmobile.sir.seguridad.rest.enums.ErrorCodeEnum;
import com.dsbmobile.sir.seguridad.rest.enums.PropertiesEnum;
import com.dsbmobile.sir.seguridad.rest.ro.base.BaseInRO;
import com.dsbmobile.sir.seguridad.rest.ro.base.BaseOutRO;
import com.dsbmobile.sir.seguridad.rest.ro.in.AutenticarInRO;
import com.dsbmobile.sir.seguridad.rest.ro.out.AutenticarOutRO;
import com.dsbmobile.sir.seguridad.rest.service.AdService;
import com.dsbmobile.sir.seguridad.rest.service.CrudService;
import com.dsbmobile.sir.seguridad.rest.service.PerfilService;
import com.dsbmobile.sir.seguridad.rest.service.UsuarioService;
import com.dsbmobile.sir.seguridad.rest.util.PropertiesUtils;
import com.dsbmobile.sir.seguridad.util.QueryParameter;
import com.dsbmobile.sir.seguridad.util.StringUtils;
import com.dsbmobile.sir.seguridad.util.enums.EstadoEnum;
import com.dsbmobile.sir.seguridad.util.exception.BusinessException;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

/**
 *
 * @author otheo
 */
@Repository(value = "usuarioService")
@Transactional
public class UsuarioServiceImpl implements UsuarioService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UsuarioServiceImpl.class);

    private static final String USUARIO_REST = PropertiesUtils.getProperty(PropertiesEnum.USUARIO_REST.getValue());

    @Autowired
    private CrudService crudService;
    @Autowired
    private PerfilService perfilService;
    @Autowired
    private AdService adService;

    @Override
    public AutenticarOutRO autenticar(AutenticarInRO in) {
        AutenticarOutRO out = new AutenticarOutRO();
        try {
            validarAutenticarInRO(out, in);
            if (!out.tieneError()) {
                AutenticacionResponseDTO autenticacionResponseDTO = this.adService.autenticar(in.getUsuario(), in.getContrasenia());
                if (autenticacionResponseDTO.tieneError()) {
                    out.setError(ErrorCodeEnum.ERROR_AUTENTICACION);
                } else {
                    out.setToken(autenticacionResponseDTO.getToken());
                }
            }
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            out.setError(ErrorCodeEnum.ERROR_GENERICO);
        }
        return out;
    }

    private void validarAutenticarInRO(AutenticarOutRO out, AutenticarInRO in) {
        if (in == null) {
            out.setError(ErrorCodeEnum.DATOS_REQUERIDOS);
            return;
        }
        if (StringUtils.isEmpty(in.getUsuario())) {
            out.setError(ErrorCodeEnum.DATOS_REQUERIDOS, "usuario requerido.");
            return;
        }
        if (StringUtils.isEmpty(in.getContrasenia())) {
            out.setError(ErrorCodeEnum.DATOS_REQUERIDOS, "contrasenia requerida.");
        }
    }

    @Override
    public BaseOutRO sincronizarUsuarios(BaseInRO in) {
        BaseOutRO out = new BaseOutRO();
        try {
            ListGrupoResponseDTO listGrupoResponseDTO = this.adService.listarGrupos();
            if (!listGrupoResponseDTO.tieneError()) {
                List<GrupoResponseDTO> gruposResponseDTO = listGrupoResponseDTO.getGrupos();
                if (CollectionUtils.isEmpty(gruposResponseDTO)) {
                    out.setError(ErrorCodeEnum.NO_HAY_DATOS);
                    out.setMessage("No hay datos a sincronizar.");
                } else {
                    for (GrupoResponseDTO grupoResponseDTO : gruposResponseDTO) {
                        Perfil perfil = this.crearGrupo(grupoResponseDTO);
                        ListUsuarioResponseDTO listUsuarioResponseDTO = this.adService.listarUsuarios(perfil.getCodPerfil(), listGrupoResponseDTO.getToken());
                        if (!listUsuarioResponseDTO.tieneError()) {
                            List<UsuarioResponseDTO> usuariosResponseDTO = listUsuarioResponseDTO.getUsuarios();
                            if (!CollectionUtils.isEmpty(usuariosResponseDTO)) {
                                for (UsuarioResponseDTO usuarioResponseDTO : usuariosResponseDTO) {
                                    this.crearUsuario(usuarioResponseDTO, perfil);
                                }
                            }
                        } else {
                            throw new BusinessException(listUsuarioResponseDTO.getMessage());
                        }
                    }
                }
            } else {
                out.setError(ErrorCodeEnum.ERROR_GENERICO, "Error al sincronizar usuarios.");
                out.setMessage(listGrupoResponseDTO.getMessage());
            }
        } catch (BusinessException ex) {
            LOGGER.error(ex.getMessage(), ex);
            out.setError(ErrorCodeEnum.ERROR_GENERICO);
            out.setMessage(ex.getMessage());
        }
        return out;
    }

    private Perfil crearGrupo(GrupoResponseDTO grupoResponseDTO) throws BusinessException {
        try {
            if (!this.perfilService.existe(grupoResponseDTO.getId())) {
                Perfil perfil = new Perfil();
                perfil.setCodPerfil(grupoResponseDTO.getId());
                perfil.setDatosAuditoria(new DatosAuditoria());
                perfil.getDatosAuditoria().auditForCreate(USUARIO_REST);
                perfil.setEstado(EstadoEnum.ACTIVO.getValue());
                perfil.setNombre(grupoResponseDTO.getDisplayName());
                return this.crudService.create(perfil);
            } else {
                return this.perfilService.findByCodigo(grupoResponseDTO.getId());
            }
        } catch (BusinessException ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw new BusinessException("Error al crear grupo. " + ex.getMessage(), ex);
        }
    }

    private void crearUsuario(UsuarioResponseDTO usuarioResponseDTO, Perfil perfil) throws BusinessException {
        try {
            if (!this.existe(usuarioResponseDTO.getId())) {
                Usuario usuario = new Usuario();
                usuario.setDatosAuditoria(new DatosAuditoria());
                usuario.getDatosAuditoria().auditForCreate(USUARIO_REST);
                usuario.setEstado(EstadoEnum.ACTIVO.getValue());
                usuario.setNombre(usuarioResponseDTO.getDisplayName());
                usuario.setPerfil(perfil);
                usuario.setUniqueId(usuarioResponseDTO.getId());
                usuario.setUsuario(usuarioResponseDTO.getUserPrincipalName());
                usuario.setCorreoElectronico(usuarioResponseDTO.getCorreoElectronico());
                this.crudService.create(usuario);
            }
        } catch (BusinessException ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw new BusinessException("Error al crear usuario. " + ex.getMessage(), ex);
        }
    }

    @Override
    public boolean existe(String uniqueId) {
        boolean result = false;
        try {
            Long cant = (Long) this.crudService.createNamedQuery(Usuario.EXISTE,
                    QueryParameter.with("uniqueId", uniqueId).parameters())
                    .getSingleResult();
            result = cant > 0;
        } catch (BusinessException ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
        return result;
    }

}
