package com.dsbmobile.sir.seguridad.rest.dto;

/**
 *
 * @author otheo
 */
public class AutenticacionResponseDTO extends BaseDTO {

    private static final long serialVersionUID = -8519517972865458647L;

    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return "AutenticacionResponseDTO{" + "token=" + token + '}';
    }

}
